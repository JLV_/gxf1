Play here: https://jlv_.gitlab.io/gxf1/

![](https://static.jam.host/raw/ccf/21/z/12741.png)

![](https://static.jam.host/raw/ccf/21/z/12746.png)

![](https://static.jam.host/raw/ccf/21/z/12747.png)

![](https://static.jam.host/raw/ccf/21/z/12870.png)

gxf1 blends a typical chess simulation with a narrative structure heavily reminiscent of visual novels. The twist : you are a pawn trying to survive in the midst of the board...

