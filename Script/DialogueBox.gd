extends Label

var t = []
var lab

# Narrator : narr
# Player pawn : bpa0
# Black tower 1 : bto1
# White pawn 1 : wpa1
# Black knight 1 : bkn1
# White pawn 2 : wpa2
# White tower 1 : wto1
# Black queen : bque
# White king : wkin
# White pawn : wpa3




func _ready():
	
	lab = get_node("dialogue_box")
	#Tour 0 : intro
	t.append(["narr", "There is a place where one's reason to live can be summarized in the following terms :"])
	t.append(["narr", "\"Annihilate the opponent.\""])
	t.append(["narr", "In a world where your doom is only five squares ahead, and your fate in the hands of an extremely fallible deity..."])
	t.append(["narr", "...could your still refer to yourself as a \"pawn of destiny\" ?"])
	
	# Player pawn appears, moves from h7 to h5
	t.append(["bpa0", "Aww man, finally ! Entering the scene at last !"])
	t.append(["bto1", "Why are you so eager to rush out there ?"])
	t.append(["bto1", "You could end up being trampled on by a knight or torn apart by a queen on a whim of our dear commander."])
	t.append(["bpa0", "That's unusual for you big pieces to worry about such a trifle."])
	t.append(["bpa0", "I mean, we pawns are all but expendable, but you could even survive a fight with the queen !"])
	t.append(["bto1", "I am just telling you to make it count."])
	t.append(["bpa0", "I'll keep that in mind. Thanks for the motivational speech, big guy !"])
	
	# Tour 1 : White pawn 1 moves from h2 to h3
	t.append(["bkn1", "*Undistinguishable mix of maddened screams and horse screeches*"])
	t.append(["wpa1", "You have some nerve, going that deep into our territory."])
	t.append(["bkn1", "*Screaming intensifies, the horse looks bewildered*"])
	t.append(["wpa1", "What a disgrace... Enough with this jest, prepare to die !"])
	
	# Player pawn moves from h4 to h3
	t.append(["bpa0", "Not so fast !"])
	t.append(["wpa1", "Oh ? That's certainly bold on your part... But tell me, what will you do now ?"])
	t.append(["wpa1", "Heck, what CAN you do ?"])
	t.append(["bpa0", "Stay where you are !"])
	t.append(["wpa1", "What if I'm ordered the opposite ? The orders of our commanders are absolute, you know ?"])
	t.append(["bpa0", "As soon as you make the tiniest movement, you're in for a one-way ticket to hell !"])
	t.append(["wpa1", "Well then, try to stop me ! I'll even proceed slowly to let you enjoy the show."])
	t.append(["bkn1", "*The knight makes pathetic moves with his legs, desperately trying to appease his panicked mount*"])
	
	# Tour 2 : White pawn 1 takes Black knight 1 (h4 to g4) (slowly)
	t.append(["bpa0", "..."])
	t.append(["wpa1", "Trivial, really. The ones put in position of winning by the rules are unstoppable."])
	t.append(["bpa0", "... ... ..."])
	t.append(["wpa1", "Serves you right. Now, on to a landslide victory !"])
	t.append(["bpa0", "...You talk too much."])
	
	# Player pawn takes White pawn 2 (h4 to g3) 
	t.append(["wpa2", "Wha- !!!"])
	t.append(["bpa0", "So long, loser."])
	t.append(["wpa1", "Tch. Aiming for the small fry, uh ? Our elite will crush you."])
	
	# Tour 3 : White tower 1 moves from h1 to g1
	t.append(["bpa0", "Speak of the devil..."])
	t.append(["wto1", "Die."])
	
	# Black tower 1 moves from h8 to h1
	t.append(["bpa0", "?! Why are you..."])
	t.append(["bto1", "Lecturing people when you were blessed with powers such as mine... "])
	t.append(["wto1", "Fool. Die."])
	t.append(["bto1", "...it makes no sense if you don't practice what you preach !"])
	
	# Tour 4 : White tower 1 takes Black tower 1 (g1 to h1)
	t.append(["bpa0", "... I get it."])
	
	# Player pawn moves from g3 to g2
	t.append(["bpa0", "..."])
	t.append(["bpa0", "Ah, seriously, I didn't need a demonstration, big guy."])
	t.append(["wto1", "Danger. Potential imminent checkmate."])
	t.append(["bpa0", "Yup, the commander really has outdone himself this time."])
	t.append(["wto1", "Threats : queen on e7, bishop on f5, pawn on g2."])
	t.append(["wto1", "Least damaging choice : moving to f1."])
	
	# Tour 5 : White tower 1 moves from h1 to f1 (cf feuille)
	t.append(["bpa0", "Yeah, you said it..."])
	t.append(["bque", "Please make room for your queen !"])
	
	# Black queen moves from f7 to h4 (echec)
	t.append(["bpa0", "...and there comes the non-braindead cavalry !"])
	t.append(["bque", "That's a check, your self-appointed Highness !"])
	t.append(["wkin", "How vulgar, how obscene ! Pardon my bluntness, your so-called majesty, but I shall uphold my pride !"])
	t.append(["wkin", "En garde !"])
	t.append(["wpa3", "Hey, pardon me your Highness, but you should really back down, we're all done for if we lose you, remember ?"])
	t.append(["wkin", "Hmm, it seems like I can't remain deaf to my subjects' complaints."])
	
	# Tour 6 : White king moves from e1 to d1 
	t.append(["wkin", "And thus, the King opts for a strategic retreat !"])
	t.append(["bpa0", "(Damn, I understand now...)"])
	t.append(["bque", "You unredeemable coward !"])
	t.append(["wto1", "King safe, pawn blocked. Defense succesful."])
	t.append(["bpa0", "What it means to make it count !"])
	
	# Player pawn takes White tower 1 (g2 to f1)
	t.append(["bque", "That's a check, now is our chance ! Do not let it go to waste !"])
	# Player pawn takes White tower 1 (g2 to f1)
	# White bishop takes player pawn (e2 to f1)
	# Black bishop takes white pawn (f5 to g4)
	# White bishop moves from f1 to e2
	t.append(["bque", "Here I come !"])
	# Black queen moves from h4 to h1
	set_process(true)

func _process(delta):
	1

func print_text(string):
	lab.visible_characters = 1
	lab.text
