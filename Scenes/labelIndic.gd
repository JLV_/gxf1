extends Sprite

# class member variables go here, for example:
# var a = 2
var playLabel
var howtoLabel
var creditsLabel
var anim
var animhow
var hownode
var timer
var main = true
var how = false
var cred = false
var first_start = true


func _ready():
		
	playLabel = get_node("../Menu/Label3")
	howtoLabel = get_node("../Menu/Label2")
	creditsLabel = get_node("../Menu/Label4") 
	anim = get_node("../Menu/AnimationPlayer")
	animhow = get_node("../How/anim")
	hownode = get_node("../How")
	self.hide()
	hownode.hide()

func _process(delta):
	self.hide()	
	if !first_start:
		anim.play_backwards("fadeout")
		first_start = true
	if main:
		if playLabel.is_mouse_in:


			self.position.x = 200
			self.position.y = 140
			if Input.is_action_just_pressed("click"):
				main = false
				anim.play("fadeout")
				_wait_change(0.1)			
		elif howtoLabel.is_mouse_in:
			self.show()
			self.position.x = 154
			self.position.y = 190
			if Input.is_action_just_pressed("click"):
				hownode.show()
				anim.play("fadeout")
				animhow.play("fadein")
				how = true
				main = false
		elif creditsLabel.is_mouse_in:
			self.show()
			self.position.x = 184
			self.position.y = 240
		else:
			self.hide()
		
func _wait_change(seconds):
	timer = get_node("../Timer")
	timer.wait_time = seconds
	timer.start()
	yield(timer,"timeout")
	get_tree().change_scene("res://scenes/tour1.tscn")
	

		
