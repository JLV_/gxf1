extends AnimatedSprite

var piecen

func _ready():
	set_piece_r("wpa2", "b3")
	set_piece_r("bpa5", "e5")
	
	set_piece_r("wbi1", "b2")

	set_piece_r("wpa3", "c4")
	set_piece_r("bkn2", "f6")	

	set_piece_r("bpa5", "e4")	

	set_piece_r("bbi2", "c5")	

	set_piece_r("bpa4", "c6")
	
	set_piece_r("wpa5", "e3")
	set_piece_r("bbi1", "f5")
	
	set_piece_r("wque", "c2")
	set_piece_r("bque", "e7")
	
	set_piece_r("wbi2", "e2")
	set_piece_r("bkin", "c8")
	set_piece_r("bto1", "d8")
	
	set_piece_r("wpa6", "f4")
	set_piece_r("bkn2", "g4")
	
	set_piece_r("wpa7", "g3")
	
	set_process(true)

func _process(delta):
	1
	

		
	
func set_piece_r(piecename, casename):
	var case_x
	var case_y
	
	if casename[0]=="a":
		case_x = 0
	elif casename[0]=="b":
		case_x = 20
	elif casename[0]=="c":
		case_x = 40
	elif casename[0]=="d":
		case_x = 60
	elif casename[0]=="e":
		case_x = 80
	elif casename[0]=="f":
		case_x = 100
	elif casename[0]=="g":
		case_x = 120
	elif casename[0]=="h":
		case_x = 140
		
	if casename[1]=="8":
		case_y = 0
	elif casename[1]=="7":
		case_y = 20
	elif casename[1]=="6":
		case_y = 40
	elif casename[1]=="5":
		case_y = 60
	elif casename[1]=="4":
		case_y = 80
	elif casename[1]=="3":
		case_y = 100
	elif casename[1]=="2":
		case_y = 120
	elif casename[1]=="1":
		case_y = 140
	
	piecen = get_node("pieces/" + piecename)
	piecen.position.x = case_x
	piecen.position.y = case_y
	
func get_case_coords(casename):
	var case_x
	var case_y
	
	if casename[0]=="a":
		case_x = 0
	elif casename[0]=="b":
		case_x = 20
	elif casename[0]=="c":
		case_x = 40
	elif casename[0]=="d":
		case_x = 60
	elif casename[0]=="e":
		case_x = 80
	elif casename[0]=="f":
		case_x = 100
	elif casename[0]=="g":
		case_x = 120
	elif casename[0]=="h":
		case_x = 140
		
	if casename[1]=="8":
		case_y = 0
	elif casename[1]=="7":
		case_y = 20
	elif casename[1]=="6":
		case_y = 40
	elif casename[1]=="5":
		case_y = 60
	elif casename[1]=="4":
		case_y = 80
	elif casename[1]=="3":
		case_y = 100
	elif casename[1]=="2":
		case_y = 120
	elif casename[1]=="1":
		case_y = 140
	
	return [case_x, case_y]
	
func change_plate_frame(piece_name, mode):
	var piecen
	if piece_name == "narr" or piece_name == "orders":
		self.frame = 0
		return
	var frame_number = 0
	if mode == 0:
		piecen = get_node("pieces/" + piece_name)
		frame_number += 64
	elif mode == 1:
		piecen = get_node(piece_name).get_rect()
		frame_number += 128
	
	if piecen.position.x==0:
		frame_number+=0
	elif piecen.position.x==20:
		frame_number+=8
	elif piecen.position.x==40:
		frame_number+=16
	elif piecen.position.x==60:
		frame_number+=24
	elif piecen.position.x==80:
		frame_number+=32
	elif piecen.position.x==100:
		frame_number+=40
	elif piecen.position.x==120:
		frame_number+=48
	elif piecen.position.x==140:
		frame_number+=56
		
	if piecen.position.y==0:
		frame_number+=1
	elif piecen.position.y==20:
		frame_number+=2
	elif piecen.position.y==40:
		frame_number+=3
	elif piecen.position.y==60:
		frame_number+=4
	elif piecen.position.y==80:
		frame_number+=5
	elif piecen.position.y==100:
		frame_number+=6
	elif piecen.position.y==120:
		frame_number+=7
	elif piecen.position.y==140:
		frame_number+=8
	self.frame = frame_number
