extends Control

var plate
var curr
var curr_x
var curr_y
var count = 0
var dialogue_box
var is_mouse_in = false

func _ready():
	plate = get_node("..")
	dialogue_box = get_node("../../dialogue_box")
	self.connect("mouse_entered", self, "on_mouse_enter")
	self.connect("mouse_exited", self, "on_mouse_exit")

	
func on_mouse_enter():
	if dialogue_box.is_playing == true:
		plate.change_plate_frame(self.name, 1)
		is_mouse_in = true

func on_mouse_exit():
	if dialogue_box.is_playing == true:
		plate.change_plate_frame("narr", 1)
		is_mouse_in = false
	
func _process(delta):
	if Input.is_action_just_pressed("click") && is_mouse_in:
		count+=1

	

