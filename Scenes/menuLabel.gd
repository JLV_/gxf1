extends Label

var is_mouse_in = false

func _ready():
	self.connect("mouse_entered", self, "on_mouse_enter")
	self.connect("mouse_exited", self, "on_mouse_exit")

	
func on_mouse_enter():
	is_mouse_in = true

func on_mouse_exit():
	is_mouse_in = false	
