extends Sprite

# class member variables go here, for example:
# var a = 2
var playLabel
var howtoLabel
var creditsLabel
var anim
var animhow
var animcred
var hownode
var crednode
var menunode
var timer
var main = true
var how = false
var cred = false
var howreturn
var credreturn


func _ready():
		
	playLabel = get_node("../Menu/Label3")
	howtoLabel = get_node("../Menu/Label2")
	creditsLabel = get_node("../Menu/Label4") 
	anim = get_node("../Menu/AnimationPlayer")
	animhow = get_node("../How/anim")
	animcred = get_node("../Cred/anim")
	hownode = get_node("../How")
	howreturn = get_node("../How/Label3")
	menunode = get_node("../Menu")
	crednode = get_node("../Cred")
	credreturn = get_node("../Cred/Label3")
	self.hide()
	hownode.hide()
	crednode.hide()

func _process(delta):	
	self.hide()
	if main:
		if playLabel.is_mouse_in:
			self.show()
			self.position.x = 200
			self.position.y = 140
			if Input.is_action_just_pressed("click"):
				main = false
				self.hide()
				anim.play("fadeout")
				_wait_change(0.1)			
		elif howtoLabel.is_mouse_in:
			self.show()
			self.position.x = 154
			self.position.y = 190
			if Input.is_action_just_pressed("click"):
				hownode.show()
				anim.play("fadeout")
				animhow.play("fadein")
				how = true
				main = false
				_wait_menu(0.1)
		elif creditsLabel.is_mouse_in:
			self.show()
			self.position.x = 184
			self.position.y = 240
			if Input.is_action_just_pressed("click"):
				crednode.show()
				anim.play("fadeout")
				animcred.play("fadein")
				cred = true
				main = false
				_wait_menu(0.1)
		else:
			self.hide()
			
	elif how:
		if howreturn.is_mouse_in:
			self.show()
			self.position.x = 374
			self.position.y = 264
			if Input.is_action_just_pressed("click"):
				menunode.show()
				anim.play_backwards("fadeout")
				animhow.play_backwards("fadein")
				how = false
				main = true
				_wait_how(0.1)
				
	elif cred:
		if credreturn.is_mouse_in:
			self.show()
			self.position.x = 374
			self.position.y = 20
			if Input.is_action_just_pressed("click"):
				menunode.show()
				anim.play_backwards("fadeout")
				animcred.play_backwards("fadein")
				cred = false
				main = true
				_wait_cred(0.1)
				
		
func _wait_change(seconds):
	timer = get_node("../Menu/Timer")
	timer.wait_time = seconds
	timer.start()
	yield(timer,"timeout")
	get_tree().change_scene("res://Scenes/tour1.tscn")

func _wait_menu(seconds):
	timer = get_node("../Menu/Timer")
	timer.wait_time = seconds
	timer.start()
	yield(timer,"timeout")
	menunode.hide()

func _wait_how(seconds):
	timer = get_node("../Menu/Timer")
	timer.wait_time = seconds
	timer.start()
	yield(timer,"timeout")
	hownode.hide()
	
func _wait_cred(seconds):
	timer = get_node("../Menu/Timer")
	timer.wait_time = seconds
	timer.start()
	yield(timer,"timeout")
	crednode.hide()

	

		
