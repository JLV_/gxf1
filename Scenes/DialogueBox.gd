extends Label

var t = []
var p = []
var c = []
var lab
var timer
var cur = 0
var movi = 0
var ord = 0
var ready = true
var text_sound
var orders_sound
var plate
var piecename
var is_moving = false
var curr_x
var curr_y
var curr_p
var next
var next_x
var next_y
var speed
var move_num = 0
var is_playing = false
var orderbox
var curr_obj
var obj_started = false
var titlebox
var gamebox_sprite
var main_pawn
var pawn_anim
var plate_anim
var titlebox_anim
var orderbox_anim
var gamebox_anim
var indic_text
var indic_text_anim
var indic_text_on = false
var indic_play
var indic_play_anim
var indic_play_on = false
var first_start = false
var bgm
var bgm_anim
var mc

# Narrator : narr
# Player pawn : bpa8
# Black tower 2 : bto2
# White pawn 8 : wpa8
# Black knight 2 : bkn2
# White pawn 7 : wpa7
# White tower 2 : wto2
# Black queen : bque
# White king : wkin
# White pawn : wpa3



func _ready():
	
	set_process(true)
	
	
	#Tour 0 : intro
	t.append(["narr", "There is a place where one's reason to live can be\nsummarized in the following terms :", 0])
	t.append(["narr", "\"Annihilate your opponent.\"", 0])
	t.append(["narr", "In a world where your doom is only five squares\nahead, and your fate in the hands of an\nextremely fallible deity...", 0])
	t.append(["narr", "...could you still refer to yourself as a \"pawn of\ndestiny\" ?", 0])	
	t.append(["bpa8", "Aww man, finally ! Entering the field at last !", 0])
	
	t.append(["orders", "Move to h5", 0])
	
	t.append(["bto2", "Why are you so eager to rush out there ?", 1])
	t.append(["bto2", "You could end up being trampled on by a knight\nor torn apart by a queen on a whim of our dear\ncommander.", 0])
	t.append(["bpa8", "That's unusual for you big shots to worry about\nsuch a trifle.", 0])
	t.append(["bpa8", "I mean, we pawns are all but expendable, but you\ncould even survive a fight with the queen !", 0])
	t.append(["bto2", "I am just telling you to make it count.", 0])
	t.append(["bpa8", "Well, I'll keep that in mind. Thanks for the\nmotivational speech, big guy !", 0])

	# Tour 1 : White pawn 1 moves from h2 to h3
	t.append(["wpa8", "Hey black knight ! You have some nerve,\ngoing that deep into our territory.", 1])
	t.append(["bkn2", " *Undistinguishable mix of maddened screams and\n horse screeches*", 0])	
	t.append(["wpa8", "What a disgrace... Enough with this jest, prepare\n to die !", 0])
	t.append(["bkn2", " *Screaming intensifies, the horse looks\n bewildered*", 0])

	t.append(["orders", "Move to h4", 0])

	# Player pawn moves from h4 to h3
	t.append(["bpa8", "Not so fast !", 1])
	t.append(["wpa8", "Oh ? That's certainly bold on your part... But tell\nme, what will you do now ?", 0])
	t.append(["wpa8", "Heck, what CAN you do now ?", 0])
	t.append(["bpa8", "Stay where you are !", 0])
	t.append(["wpa8", "What if I'm ordered the opposite ? The orders of\nour commanders are absolute, you know ?", 0])
	t.append(["bpa8", "As soon as you make the tiniest movement,\nyou're in for a one-way ticket to hell !", 0])
	t.append(["bkn2", " *The knight makes pathetic moves with his legs,\n desperately trying to appease his panicked\n mount*", 0])
	t.append(["wpa8", "Well then, try to stop me ! I'll even proceed slowly\nto let you enjoy the show.", 0])
	
	# Tour 2 : White pawn 1 takes Black knight 1 (h4 to g4) (slowly)
	t.append(["bpa8", " ...", 1])
	t.append(["wpa8", "Trivial, really. The ones put in position of winning\nby the rules are unstoppable.", 0])
	t.append(["bpa8", " ... ... ...", 0])
	t.append(["wpa8", "Serves you right. Now, on to a landslide victory !", 0])	
	t.append(["bpa8", " ...You talk too much.", 0])
	
	t.append(["orders", "Take white pawn\non g3", 0])

	# Player pawn takes White pawn 2 (h4 to g3)
	t.append(["wpa8", "Wha- !!!", 1])
	t.append(["bpa8", "So long, loser.", 0])
	t.append(["wpa8", "Tch. Aiming for the small fry, uh ? Our elite will\ncrush you.", 0])

	# Tour 3 : White tower 1 moves from h1 to g1
	t.append(["bpa8", "Speak of the devil...", 1])
	t.append(["wto2", "Die.", 0])

	# Black tower 1 moves from h8 to h1
	t.append(["bpa8", "?! Why are you...", 1])
	t.append(["bto2", "Lecturing people when you were blessed with\npowers such as mine... ", 0])
	t.append(["wto2", "Fool. Die.", 0])
	t.append(["bto2", "...it makes no sense if you don't practice what you\npreach !", 0])

	# Tour 4 : White tower 1 takes Black tower 1 (g1 to h1)
	t.append(["bpa8", "...", 1])
	t.append(["bpa8", "Damn, seriously, I didn't need a demonstration,\nbig guy.", 0])
	
	t.append(["orders", "Move to g2", 0])
	
	# Player pawn moves from g3 to g2
	t.append(["wto2", "Danger. Potential imminent checkmate.", 1])
	t.append(["wto2", "Threats : queen on e7, bishop on f5, pawn on g2.", 0])
	t.append(["bpa8", "Yup, the commander really has outdone himself\nthis time.", 0])
	t.append(["wto2", "Least damaging choice : moving to f1.", 0])

	# Tour 5 : White tower 1 moves from h1 to f1 (cf feuille)
	t.append(["bpa8", "You said it !", 1])
	t.append(["bque", "Please make room for your queen !", 0])

	# Black queen moves from f7 to h4 (echec)
	t.append(["bpa8", "...and there comes the non-braindead cavalry !", 1])
	t.append(["bque", "That's a check, your self-appointed Highness !", 0])
	t.append(["wkin", "How vulgar, how obscene ! Pardon my\nbluntness, your so-called majesty, but I shall\nuphold my pride !", 0])
	t.append(["wkin", "En garde !", 0])
	t.append(["wpa3", "Hey, pardon me your Highness, but you should\nreally back down, we're all done for if we lose\nyou, remember ?", 0])
	t.append(["wkin", "Hmm, it seems like I can't remain deaf to my\nsubjects' complaints.", 0])

	# Tour 6 : White king moves from e1 to d1
	t.append(["wkin", "And thus, the King opts for a strategic retreat !", 1])
	t.append(["bque", "You unredeemable coward !", 0])
	
	t.append(["orders", "Take white tower\non f1 and get\npromoted to queen", 0])
	
	t.append(["bpa8", "(Ah, I understand now...)", 0])
	t.append(["wto2", "King safe, pawn blocked. Defense successful.", 0])
	
	# Player pawn takes White tower 1 (g2 to f1)
	t.append(["bpa8", "What it means to make it count !",1])
	
	t.append(["bque", "That's a check, now is our chance ! Do not let it\ngo to waste !", 0])
	# White bishop takes player pawn (e2 to f1)
	# Black bishop takes white pawn (f5 to g4)
	# White bishop moves from f1 to e2
	t.append(["bque", "Here I come !", 3])
	# Black queen moves from h4 to h1
	t.append(["wkin", "A horse ! A horse ! My kingdom for a horse !", 1])
	t.append(["wpa3", "... Jeez, your Highness, don't go repeating\nstuff you read online just because it sounds cool !", 0])
	
	p.append(["bpa8", "h5", 1])
	p.append(["wpa8", "h3", 1])
	p.append(["bpa8", "h4", 1])
	p.append(["wpa8", "g4", 0.25])
	p.append(["bpa8", "g3", 1])
	p.append(["wto2", "g1", 1])
	p.append(["bto2", "h1", 4])
	p.append(["wto2", "h1", 1])
	p.append(["bpa8", "g2", 1])
	p.append(["wto2", "f1", 1])
	p.append(["bque", "h4", 1])
	p.append(["wkin", "d1", 1])
	p.append(["bpa8", "f1", 1])
	p.append(["wbi2", "f1", 1])
	p.append(["bbi1", "g4", 1])
	p.append(["wbi2", "e2", 1])
	p.append(["bque", "h1", 1])
	
	c.append("h5")
	c.append("h4")
	c.append("g3")
	c.append("g2")
	c.append("f1")
	
	timer = get_node("dialogue_time")
	
	text_sound = get_node("dialogue_sound")
	plate = get_node("../plate")
	orderbox = get_node("../orderbox")
	titlebox = get_node("../titlebox")
	gamebox_sprite = get_node("../gamebox_sprite")
	orders_sound = get_node("../orderbox/orders_sound")
	main_pawn = get_node("../main_pawn")
	pawn_anim = get_node("../main_pawn/anim")
	plate_anim = get_node("../plate/anim")
	titlebox_anim = get_node("../titlebox/anim")
	orderbox_anim = get_node("../orderbox/anim")
	gamebox_anim = get_node("../gamebox_sprite/anim")
	indic_text = get_node("../textbox_sprite/Sprite")
	indic_text_anim = get_node("../textbox_sprite/Sprite/anim")
	indic_play = get_node("../textbox_sprite/Sprite2")
	indic_play_anim = get_node("../textbox_sprite/Sprite2/anim")
	bgm = get_node("music")
	bgm_anim = get_node("music/anim")
	mc = get_node("../plate/pieces/bpa8")
	
	indic_text.hide()
	indic_play.hide()


func _process(delta):

	if !first_start:
		get_node("../anim").play("fadein")
		first_start = true
		_wait_start(0.2)
	if ready == true && !indic_text_on && !is_playing:
		indic_text.show()
		indic_text_anim.play("move")
		indic_text_on = true
	if is_playing:
		if !indic_play_on:
			indic_play.show()
			indic_play_anim.play("move")
			indic_play_on = true
		curr_obj = get_node("../plate/" + c[ord])
		if obj_started == false:
			obj_started = true
			curr_obj.set_process(true)
		if curr_obj.count > 0:
			obj_started = false
			curr_obj.set_process(false)
			is_playing = false
			orderbox.text = ""
			indic_play.hide()
			indic_play_anim.stop()
			indic_play_on = false
			curr_obj.count = 0
			ord += 1
	if ready == true and Input.is_action_just_pressed("click") && is_moving == false && is_playing == false:
		indic_text.hide()
		indic_text_anim.stop()
		indic_text_on = false
		if cur < t.size():
			move_num = t[cur][2]
			if move_num == 0 && t[cur][0] != "orders":
				plate.change_plate_frame(t[cur][0], 0)
				print_text(t[cur][1], 0.0005, 0, 1)
				cur+=1
				ready = false
			elif move_num == 0 && t[cur][0] == "orders":
				is_playing = true
				print_orders(t[cur][1], 0.0005, 0, 1)
				cur+=1
				ready = false
			if cur == 2:
				pawn_anim.play("fadein")
			if cur == 4:
				plate_anim.play("fadein")
				titlebox_anim.play("fadein")
				orderbox_anim.play("fadein")
				gamebox_anim.play("fadein")
			if cur == 5:
				main_pawn.hide()
				bgm_anim.play("fadein")
				bgm.play()
			if cur == 62:
				mc.frame = 1
		if cur > t.size()+1:
			get_node("../anim").play_backwards("fadein")
			bgm_anim.play_backwards("fadein")			
			_wait_change(0.1)			
		if cur == t.size()+1:
			plate_anim.play_backwards("fadein")
			titlebox_anim.play_backwards("fadein")
			orderbox_anim.play_backwards("fadein")
			gamebox_anim.play_backwards("fadein")
			print_text("Thanks for playing !", 0.0005, 0, 1)
			cur += 1
			ready = false
		if cur == t.size():
			print_orders("CHECKMATE :\nBlack player wins", 0.0005, 0, 1)
			cur+=1

					
	while move_num > 0 && is_moving == false:
		is_moving = true
		curr_p = get_node("../plate/pieces/" + p[movi][0])
		curr_x = curr_p.position.x
		curr_y = curr_p.position.y
		next = plate.get_case_coords(p[movi][1])
		next_x = next[0]
		next_y = next[1]
		speed = p[movi][2]
		movi += 1
		move_num -= 1
		if movi == 4:
			get_node("../plate/pieces/" + "bkn2").visible = false
		if movi == 5:
			get_node("../plate/pieces/" + "wpa7").visible = false
		if movi == 8:
			get_node("../plate/pieces/" + "bto2").visible = false
		if movi == 13:
			get_node("../plate/pieces/" + "wto2").visible = false
		if movi == 14:
			get_node("../plate/pieces/" + "bpa8").visible = false
		if movi == 15:
			get_node("../plate/pieces/" + "wpa8").visible = false		
	if is_moving: 
		curr_x = curr_p.position.x
		curr_y = curr_p.position.y
		if curr_x == next_x and curr_y == next_y:			
			if cur < t.size() and ready == true and move_num == 0:
				ready = false
				plate.change_plate_frame(t[cur][0], 0)
				print_text(t[cur][1], 0.0005, 0, 1)
				cur+=1
			is_moving = false
		if curr_x > next_x:
			curr_p.position.x -= speed
		elif curr_x < next_x:
			curr_p.position.x += speed
		if curr_y > next_y:
			curr_p.position.y -= speed
		elif curr_y < next_y:
			curr_p.position.y += speed

		
		

func print_text(string, speed, number, space_sound):
	self.visible_characters = number
	if !text_sound.playing && float(number)/float(string.length()) < 0.8:
		text_sound.play()	
	self.text = string	
	timer.wait_time = speed
	timer.start()
	yield(timer, "timeout")	
	if self.visible_characters != string.length():
		print_text(string, speed, number+1, space_sound)
	else:
		ready = true
		
func print_orders(string, speed, number, space_sound):
	orderbox.visible_characters = number
	if !orders_sound.playing && float(number)/float(string.length()) < 0.8:
		orders_sound.play()	
	orderbox.text = string	
	timer.wait_time = speed
	timer.start()
	yield(timer, "timeout")	
	if orderbox.visible_characters != string.length():
		print_orders(string, speed, number+1, space_sound)
	else:
		ready = true
		
func _wait_change(seconds):
	timer.wait_time = seconds
	timer.start()
	yield(timer,"timeout")
	bgm.stop()
	get_tree().change_scene("res://Scenes/menu.tscn")
	
func _wait_start(seconds):
	timer.wait_time = seconds
	timer.start()
	yield(timer,"timeout")
	print_text(t[cur][1], 0.0005, 0, 1)
	cur+=1
	ready = false
	

	
